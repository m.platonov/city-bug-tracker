import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
  theme: {
    'primary': '#1976d2',
    'secondary': '#3f51b5',
    'accent': '#4caf50',
    'error': '#e53935',
    'info': '#2196F3',
    'success': '#4CAF50',
    'warning': '#FB8C00'
  },
  iconfont: 'md'
});
