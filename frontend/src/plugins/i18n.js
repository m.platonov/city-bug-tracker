import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VeeValidate from 'vee-validate'
import validationMessages from 'vee-validate/dist/locale/ru';

import ru from '@/locales/ru'

Vue.use(VueI18n);

Vue.use(VeeValidate, {
  locale: 'ru',
  dictionary: {
    ru: validationMessages
  }
});

export default new VueI18n({
  locale: 'ru',
  fallbackLocale: 'ru',
  messages: { ru }
})
