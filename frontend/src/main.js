import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import { store } from '@/store'
import '@/plugins/axios'
import '@/plugins/vuetify'
import i18n from '@/plugins/i18n'
import './registerServiceWorker'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  render: function (h) { return h(App) }
}).$mount('#app')

