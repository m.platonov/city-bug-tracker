import axios from 'axios'

export default {
  getMyTasks (filters) {
    return axios.get('/api/tasks/my', {
      params: {
        ...filters
      }
    })
  },
  getTasks (filters) {
    return axios.get('/api/tasks', {
      params: {
        ...filters
      }
    })
  },
  getPublicTasks (filters) {
    return axios.get('/api/tasks/public', {
      params: {
        ...filters
      }
    })
  },
  followToTask (id) {
    return axios.post('/api/tasks/follow/' + id)
  },
  addRateToTask (taskId, score) {
    return axios.post('/api/tasks/rate', { taskId, score })
  },
  getTaskById (id) {
    return axios.get('/api/task/' + id)
  },
  createTask (task) {
    return axios.post('/api/tasks', task)
  }
}
