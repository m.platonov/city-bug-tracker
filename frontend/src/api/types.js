import axios from 'axios'

export default {
  getTypes () {
    return axios.get('/api/types')
  }
}
