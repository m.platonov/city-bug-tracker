import axios from 'axios'

export default {
  userLogin (credentials) {
    return axios.post('/auth/token', credentials)
  },
  userSignUp (credentials) {
    return axios.post('/auth/signup', credentials)
  }
}
