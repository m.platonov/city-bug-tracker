import axios from 'axios'

export default {
  getUser () {
    return axios.get('/api/users/current')
  },
  saveProfile (profile) {
    return axios.put('/api/users/current/profile', profile)
  }
}
