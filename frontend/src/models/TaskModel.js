import { TypeModel } from '@/models/TypeModel'

export class TaskModel {
  constructor (task = {}) {
    this.id = task.id;
    this.name = task.name;
    this.status = task.status;
    this.type = new TypeModel(task.type);
    this.description = task.description;
    this.createdAt = task.createdAt;
    this.creator = task.creator
    this.deadlineAt = task.deadlineAt
    this.executor = task.executor
    this.images = []
    this.rating = null
    this.followerList = task.followerList
    this.scoreTotal = task.scoreTotal
  }

  static getFilters () {
    return {
      limit: 5,
      page: 1,
      status: 'draft'
    }
  }

  static getGetStatusByAlias (alias) {
    return TaskModel.getStatuses().find(status=> status.alias === alias)
  }

  static getStatuses () {
    return [
      {
        name: 'Голосование',
        alias: 'draft',
        icon: 'supervisor_account'
      },
      {
        name: 'Назначена',
        alias: 'assigned',
        icon: 'assignment_turned_in'
      },
      {
        name: 'В работе',
        alias: 'in_progress',
        icon: 'work'
      },
      {
        name: 'Исполнена',
        alias: 'executor_done',
        icon: 'check'
      },
      {
        name: 'На контроле',
        alias: 'control',
        icon: 'report_problem'
      },
      {
        name: 'В архиве',
        alias: 'archive',
        icon: 'archive'
      }
    ]
  }
}
