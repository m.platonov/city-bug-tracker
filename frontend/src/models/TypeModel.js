export class TypeModel {
  constructor (type = {}) {
    this.id = type.id;
    this.name = type.name;
    this.isActual = type.isActual;
  }
}
