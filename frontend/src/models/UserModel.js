export class UserModel {
  constructor (user = {}) {
    this.id = user.id;
    this.username = user.username;
    this.phone = user.login;
    this.type = 'citizen';
    this.email = user.email;
    this.createdAt = user.createdAt;
  }
}
