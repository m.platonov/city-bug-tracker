import Vue from 'vue';
import Router from 'vue-router'
import Meta from 'vue-meta'
import { store } from '@/store'
import routes from '@/router/routes/index'

Vue.use(Router);
Vue.use(Meta);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [...routes]
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => !record.meta.skipAuth);
  const isTokenSet = store.getters.isTokenSet;
  if (requiresAuth && !isTokenSet) {
    return next('/login');
  }

  if (localStorage.user !== undefined && localStorage.user.role !== undefined) {
    const user = JSON.parse(localStorage.user);
    const hasNoAccessToRoute = to.matched.some(record => record.meta !== {} && record.meta.role === user.role);
    if (hasNoAccessToRoute) return next('/');
  }


  return next();
});

export default router
