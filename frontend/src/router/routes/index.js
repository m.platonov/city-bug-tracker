import Layout from '@/components/core/Layout';

const requireRoute = require.context('.', false, /\.js$/);
let routes = [];
let routesWithLayout = [
  {
    path: '/',
    redirect: '/tasks',
    component: Layout,
    children: []
  }
];
const requiredRoutesFromLayout = require.context('./layout/', false, /\.js$/);

requiredRoutesFromLayout.keys().forEach(fileName => {
  let route = requiredRoutesFromLayout(fileName).default;
  routesWithLayout[0].children.push(...route)
});
requireRoute.keys().forEach(fileName => {
  if (fileName === './index.js') {
    return
  }
  let route = requireRoute(fileName).default;
  routes.push(...route)
});

export default routes.concat(routesWithLayout)
