import UserTasks from '@/views/tasks/UserTasks'
import ViewTask from '@/views/tasks/ViewTask'
import PublicTasks from '@/views/tasks/PublicTasks'
import EstimateTasks from '@/views/tasks/EstimateTasks'
import EstimateTask from '@/views/tasks/EstimateTask'

export default [
  {
    path: '/tasks',
    name: 'tasks',
    component: UserTasks
  },
  {
    path: '/public-tasks',
    name: 'public-tasks',
    component: PublicTasks
  },
  {
    path: '/estimate-tasks',
    name: 'estimate-tasks',
    component: EstimateTasks
  },
  {
    path: '/tasks/detail/:id',
    name: 'task-detail',
    component: ViewTask
  },
  {
    path: '/tasks/estimate/:id',
    name: 'task-estimate',
    component: EstimateTask
  }
]
