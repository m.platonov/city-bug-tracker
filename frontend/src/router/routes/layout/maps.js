import Maps from '@/views/Maps'

export default [
  {
    path: '/maps',
    name: 'maps',
    component: Maps
  }
]
