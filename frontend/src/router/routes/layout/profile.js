import Profile from '@/views/Profile';

export default [
  {
    path: '/profile',
    name: 'profile',
    component: Profile
  }
]
