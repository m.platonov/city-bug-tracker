import Tasks from '@/views/controller/Tasks'

export default [
  {
    path: '/controller/tasks',
    name: 'controller-tasks',
    component: Tasks
  }
]
