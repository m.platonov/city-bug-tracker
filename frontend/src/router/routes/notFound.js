export default [
  {
    path: '*',
    name: 'notFound',
    component: () => import('@/components/NotFound.vue')
  }
]
