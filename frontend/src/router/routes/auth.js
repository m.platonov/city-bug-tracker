import SignUp from '@/views/auth/SignUp';
import Login from '@/views/auth/Login';

export default [
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      skipAuth: true
    }
  },
  {
    path: '/signup',
    name: 'signup',
    component: SignUp,
    meta: {
      skipAuth: true
    }
  }
]
