import api from '@/api/types'
import { handleError } from '@/utils/handleError'
import { TypeModel } from '@/models/TypeModel';

const state = {
  types: []
};

const getters = {
  types: state => state.types,
  typeId: state => param => state.types.find(
    type => type.id === param)
};

const actions = {
  async getTypes ({ commit }) {
    commit('showLoading', true);
    try {
      let result = await api.getTypes();
      let types = result.data.map(
        (type) => new TypeModel(type));
      commit('setTypes', types);
      commit('showLoading', false);
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  }
};

const mutations = {
  setTypes (state, types) {
    state.types = types
  }
};

export default {
  state,
  getters,
  actions,
  mutations
}
