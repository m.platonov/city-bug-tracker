import router from '@/router'
import auth from '@/api/auth'
import profile from '@/api/user'
import { buildSuccess } from '@/utils/buildSuccess'
import { handleError } from '@/utils/handleError'
import { jwtDecode } from '@/utils/jwtDecoder'
import { UserModel } from '@/models/UserModel'

const state = {
  user: {},
  token: JSON.parse(!!localStorage.getItem('token')) || null,
  isTokenSet: !!localStorage.getItem('token')
};

const getters = {
  user: state => state.user,
  token: state => state.token,
  isTokenSet: state => state.isTokenSet
};

const actions = {
  async getToken ({ commit }, credentials) {
    commit('showLoading', true);
    try {
      let token = await auth.userLogin(credentials);
      token = token.data.token;
      let decoded = jwtDecode(token);
      window.localStorage.setItem(
        'token',
        JSON.stringify(token)
      );
      window.localStorage.setItem(
        'tokenExpiration',
        decoded.exp
      );
      commit('saveToken', token);
      buildSuccess(commit);
      router.push({ name: 'tasks' })
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async getUser ({ commit }) {
    commit('showLoading', true);
    try {
      let user = await profile.getUser();
      user = new UserModel(user.data);
      window.localStorage.setItem('user', JSON.stringify(user));
      commit('saveUser', user);
      buildSuccess(commit)
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async userSignUp ({ commit }, payload) {
    commit('showLoading', true);
    try {
      let user = await auth.userSignUp(payload);
      user = new UserModel(user.data);
      window.localStorage.setItem('user', JSON.stringify(user));
      commit('saveUser', user);
      buildSuccess(commit, {
        msg: 'signup.VERIFY_YOUR_ACCOUNT'
      })
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async updateProfile ({ commit }, payload) {
    try {
      await profile.saveProfile(payload);
      buildSuccess(commit, {
        msg: 'myProfile.PROFILE_SAVED_SUCCESSFULLY'
      })
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  userLogout ({ commit }) {
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('tokenExpiration');
    window.localStorage.removeItem('user');
    commit('logout');
    commit('saveUser', null);
    router.push({
      name: 'login'
    })
  }
};

const mutations = {
  saveToken (state, token) {
    state.token = token;
    state.isTokenSet = true
  },
  logout (state) {
    state.user = null;
    state.token = null;
    state.isTokenSet = false
  },
  saveUser (state, user) {
    state.user = user
  }
};

export default {
  state,
  getters,
  actions,
  mutations
}
