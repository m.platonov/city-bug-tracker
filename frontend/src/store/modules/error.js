const state = {
  showErrorMessage: false,
  errorMessage: null
};

const getters = {
  showErrorMessage: state => state.showErrorMessage,
  errorMessage: state => state.errorMessage
};

const mutations = {
  setError (state, payload) {
    if (payload === null) {
      state.showErrorMessage = false;
      state.errorMessage = null
    } else {
      state.showErrorMessage = true;
      state.errorMessage = payload
    }
  },
  showError (state, payload) {
    state.showErrorMessage = payload
  }
};

export default {
  state,
  getters,
  mutations
}
