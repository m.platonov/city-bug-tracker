const state = {
  appTitle: 'CityControl'
};

const getters = {
  appTitle: state => state.appTitle
};

export default {
  state,
  getters
}
