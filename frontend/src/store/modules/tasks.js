import api from '@/api/tasks'
import { handleError } from '@/utils/handleError'
import { TaskModel } from '@/models/TaskModel';
import { buildSuccess } from '@/utils/buildSuccess';

const state = {
  tasks: []
};

const getters = {
  tasks: state => state.tasks,
  taskId: state => param => state.tasks.find(
    task => task.id === param)
};

const actions = {
  async getMyTasks ({ commit }, filters = {}) {
    commit('showLoading', true);
    try {
      let result = await api.getMyTasks(filters);
      let tasks = result.data.data.map(
        (task) => new TaskModel(task));
      commit('setTasks', tasks);
      commit('showLoading', false);
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async getTasksForEstimate ({ commit }, filters = {}) {
    commit('showLoading', true);
    try {
      filters.status = 'executor_done'
      let result = await api.getTasks(filters);
      let tasks = result.data.data.map(
        (task) => new TaskModel(task));
      commit('setTasks', tasks);
      commit('showLoading', false);
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async getAllTasks ({ commit }, filters = {}) {
    commit('showLoading', true);
    try {
      let result = await api.getTasks(filters);
      let tasks = result.data.data.map(
        (task) => new TaskModel(task));
      commit('setTasks', tasks);
      commit('showLoading', false);
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async getPublicTasks ({ commit }, filters = {}) {
    commit('showLoading', true);
    try {
      let result = await api.getPublicTasks(filters);
      let tasks = result.data.data.map(
        (task) => new TaskModel(task));
      commit('setTasks', tasks);
      commit('showLoading', false);
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async getTaskById ({ commit }, id = '') {
    commit('showLoading', true);
    try {
      let result = await api.getTaskById(id);
      return new TaskModel(result.data)
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async estimateTask ({ commit }, estimateObject = {}) {
    commit('showLoading', true);
    try {
      let result = await api.addRateToTask(estimateObject.id, estimateObject.rate);
      buildSuccess(commit, {
        msg: 'К Вашему рейтингу прибавлено 10 баллов'
      })
      return new TaskModel(result.data)
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async followToTask ({ commit }, id = '') {
    commit('showLoading', true);
    try {
      await api.followToTask(id);
      buildSuccess(commit, {
        msg: 'Вы успешно поддержали жалобу'
      })
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  },
  async saveTask ({ commit }, payload) {
    commit('showLoading', true);
    try {
      let result = await api.createTask(payload);
      buildSuccess(commit, {
        msg: 'Жалоба успешно создана'
      })
      return new TaskModel(result.data)
    } catch (error) {
      handleError(commit, error);
      throw error
    }
  }
};

const mutations = {
  cleanTasks () {
    state.tasks = []
  },
  setTasks (state, tasks) {
    state.tasks = tasks
  }
};

export default {
  state,
  getters,
  actions,
  mutations
}
