const state = {
  navDrawer: false
};

const getters = {
  navDrawer: state => state.navDrawer
};

const mutations = {
  setNavDrawer (state, value) {
    state.navDrawer = value
  }
};

export default {
  state,
  getters,
  mutations
}
