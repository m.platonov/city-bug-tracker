const state = {
  showLoading: false
};

const getters = {
  showLoading: state => state.showLoading
};

const mutations = {
  showLoading (state, value) {
    state.showLoading = value
  }
};
export default {
  state,
  getters,
  mutations
}
