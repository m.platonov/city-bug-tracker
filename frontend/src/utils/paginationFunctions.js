export const paginationFunctions = (page = 1, limit = 5) => {
  return {
    offset: (page - 1) * limit,
    limit
  }
};
export const preparePaginationData = (items, total, limit = 5) => {
  return {
    currentPage: Math.ceil(items / limit) | 1,
    pages: Math.ceil(total / limit) | 0,
    total,
    limit
  }
};
