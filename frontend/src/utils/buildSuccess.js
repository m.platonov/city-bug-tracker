export const buildSuccess = (
  commit,
  msg = null
) => {
  commit('showLoading', false);
  commit('setSuccess', null);
  setTimeout(() => {
    msg ? commit('setSuccess', msg) : commit('showSuccess', false);
  }, 0);
  commit('setError', null)
};
