import { store } from '@/store'

export const checkIfTokenNeedsRefresh = () => {
  const token = window.localStorage.getItem('token');
  const tokenExpiration = window.localStorage.getItem('tokenExpiration');
  if (token !== null && tokenExpiration !== null) {
    let tokenExpirationDate = new Date(
      window.localStorage.getItem('tokenExpiration') * 1000);
    if (tokenExpirationDate - Date.parse(new Date()) < 0) {
      store.dispatch('refreshToken')
    }
  }
};
