import i18n from '@/plugins/i18n'

export const getMenuItems = (isTokenSet, type = 'citizen') => {
  let links = [
    {
      title: i18n.t('menuItems.AUTH'),
      link: 'login',
      icon: 'lock',
      class: 'btnLogin'
    },
    {
      title: i18n.t('menuItems.SIGNUP'),
      link: 'signup',
      icon: 'add_circle_outline',
      class: 'btnLogin'
    }
  ];
  if (isTokenSet && type === 'citizen') {
    links = [
      {
        title: i18n.t('menuItems.PROFILE'),
        link: 'profile',
        icon: 'person',
        class: 'btnProfile'
      },
      {
        title: i18n.t('menuItems.TASKS'),
        link: 'tasks',
        icon: 'face',
        class: 'btnTasks'
      }, {
        title: i18n.t('menuItems.PUBLIC_TASKS'),
        link: 'public-tasks',
        icon: 'public',
        class: 'btnPublicTasks'
      },
      {
        title: i18n.t('menuItems.ESTIMATES'),
        link: 'estimate-tasks',
        icon: 'grade',
        class: 'btnEstimateTasks'
      }
    ]
  }
  if (isTokenSet && type === 'controller') {
    links = [
      {
        title: i18n.t('menuItems.DASHBOARD'),
        link: 'dashboard',
        icon: 'dashboard',
        class: 'btnDashboard'
      },
      {
        title: i18n.t('menuItems.TASKS'),
        link: 'controller-tasks',
        icon: 'face',
        class: 'btnTasks'
      }
    ]
  }
  return links
};
