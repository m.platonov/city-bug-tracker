import { store } from '@/store'

export const handleError = (commit, error = null) => {
  if (error.response !== undefined && error.response.status === 401) {
    commit('logout', false);
    localStorage.removeItem('token');
    localStorage.removeItem('tokenExpiration');
    localStorage.removeItem('user');
    window.location.replace('/login');
  }


  let errMsg = error.response
    ? error.response.data.data
    : 'SERVER_TIMEOUT_CONNECTION_ERROR';
  commit('showLoading', false);
  commit('setError', null);

  switch (errMsg) {
    case 'JWT_TOKEN_IS_INVALID':
      store.dispatch('userLogout');
      break;
  }

  setTimeout(() => {
    errMsg ? commit('setError', errMsg) : commit('showError', false)
  }, 0)
};
