export default {
  sources: {
    TITLE: 'Источники'
  },
  errors: {
    BAD_TOKEN: 'Bad token',
    WRONG_PASSWORD: 'Wrong password',
    USER_DOES_NOT_EXIST: 'User does not exists',
    PASSWORDS_DONT_MATCH: 'Passwords don´t match',
    PASSWORD_TOO_SHORT_MIN_5: 'Password too short, minimum 5 characters',
    BLOCKED_USER: 'Too many failed login attempts, user blocked temporarely',
    SERVER_TIMEOUT_CONNECTION_ERROR: 'Server is not responding, connection error',
    404: '404 Page not found',
    EMAIL_IS_NOT_VALID: 'E-mail not valid',
    IS_EMPTY: 'Empty fields',
    ERROR: 'Error',
    MISSING: 'Missing fields',
    NOT_FOUND_OR_ALREADY_VERIFIED: 'Not found or already verified',
    NOT_FOUND_OR_ALREADY_USED: 'Not found or already used',
    NOT_FOUND: 'Not found',
    UNAUTHORIZED: 'Unauthorized',
    EMAIL_ALREADY_EXISTS: 'E-mail already exists',
    ID_MALFORMED: 'ID malformed',
    ERROR_WITH_FILTER: 'Error with filter',
    CITY_ALREADY_EXISTS: 'City already exists'
  },
  common: {
    DELETE: 'Delete',
    CANCEL: 'Cancel',
    WARNING: 'Warning',
    DO_YOU_REALLY_WANT_TO_DELETE_THIS_ITEM: 'Do you really want to delete this item?',
    CLOSE: 'Close',
    SAVED_SUCCESSFULLY: 'Saved successfully',
    DELETED_SUCCESSFULLY: 'Deleted successfully',
    CREATED: 'Created',
    UPDATED: 'Updated',
    GO_BACK: 'Вернуться'
  },
  menuItems: {
    DASHBOARD: 'Дашборд',
    TASKS: 'Личные Жалобы',
    PUBLIC_TASKS: 'Публичные Жалобы',
    ESTIMATES: 'Оценки',
    NOTIFICATIONS: 'Уведомления',
    PROFILE: 'Профиль',
    SIGNUP: 'Регистрация',
    AUTH: 'Вход',
    LOGOUT: 'Выход'
  },
  dataTable: {
    NEW_ITEM: 'New Item',
    EDIT_ITEM: 'Edit Item',
    CANCEL: 'Cancel',
    SAVE: 'Save',
    EDIT: 'Edit',
    DELETE: 'Delete',
    RESET: 'Reset',
    ACTIONS: 'Actions',
    ROWS_PER_PAGE: 'Rows per page',
    NO_DATA: 'No data',
    NO_RESULTS: 'No results',
    OF: 'of',
    SEARCH: 'Search'
  },
  users: {
    TITLE: 'Users',
    headers: {
      NAME: 'Name',
      EMAIL: 'E-mail',
      ROLE: 'Role',
      VERIFIED: 'Verified',
      CITY: 'City',
      COUNTRY: 'Country',
      PHONE: 'Phone'
    },
    PASSWORD: 'Password',
    CONFIRM_PASSWORD: 'Confirm Password'
  },
  home: {
    TITLE: 'Protected Home',
    CLOSE: 'Close'
  },
  about: {
    TITLE: 'About',
    DESCRIPTION: 'This page is publicly available'
  },
  login: {
    TITLE: 'Вход',
    EMAIL: 'E-mail',
    PHONE: 'Телефон',
    PASSWORD: 'Пароль',
    FORGOT_PASSWORD: 'Забыли пароль?'
  },
  statuses: {
    draft: 'На голосовании',
    assigned: 'Назначена на исполнителя',
    in_progress: 'В работе',
    executor_done: 'Исполнена',
    control: 'На контроле',
    archive: 'В архиве'
  },
  signup: {
    TITLE: 'Регистрация',
    NAME: 'Фио',
    EMAIL: 'E-mail',
    PHONE: 'Телефон',
    PASSWORD: 'Пароль',
    CONFIRM_PASSWORD: 'Подтвердите пароль',
    SIGN_ME_UP: 'Создать аккаунт',
    VERIFY_YOUR_ACCOUNT: 'You must verify your account by clicking the link sent to your E-mail to be able to enjoy all our app features'
  },
  verify: {
    TITLE: 'E-mail верификация',
    EMAIL_VERIFIED: 'E-mail подтверждён!'
  },
  myProfile: {
    TITLE: 'My profile',
    FORM_TITLE: 'Редактировать профиль',
    FORM_TITLE_SUBTITLE: 'Пользователь',
    FULLNAME: 'Фио',
    EMAIL: 'E-mail',
    PHONE: 'Телефон',
    PROFESSION: 'Profession',
    GENDER: 'Gender',
    SAVE: 'Save',
    PROFILE_SAVED_SUCCESSFULLY: 'Профиль успешно обновлён.'
  }
}
