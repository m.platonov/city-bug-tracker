city_bug-start () {
    docker-compose up --build -d
}

alias city_bug-down="docker-compose down"

alias city_bug-restart="docker-compose build && docker-compose up -d"

city_bug-rebase() {

    echo 'drop database'
	docker-compose exec php php bin/console doctrine:database:drop --force

	echo 'create database'
	docker-compose exec php php bin/console doctrine:database:create --no-interaction

	echo 'start migrate'

	docker-compose exec php php bin/console doctrine:schema:update --force --no-interaction

	echo 'database cleanup'

	echo 'fill database'

	docker-compose exec php php bin/console doctrine:fixtures:load --no-interaction
}

alias city_bug-update-schema="docker-compose exec php php bin/console doctrine:schema:update --force --no-interaction"

alias city_bug-in="docker-compose exec php bash"

city_bug-jwt-create () {
    rm -rf config/jwt/ && mkdir config/jwt/
    openssl genrsa -out config/jwt/private.pem -passout pass:bc2f00af43a690244606586c27eaf1d4 -aes256 4096
    openssl rsa -passin pass:bc2f00af43a690244606586c27eaf1d4 -pubout -in config/jwt/private.pem -out  config/jwt/public.pem
    chmod 777 -R config/jwt/
}
