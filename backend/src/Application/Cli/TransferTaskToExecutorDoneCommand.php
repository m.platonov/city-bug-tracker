<?php

namespace App\Application\Cli;


use App\Domain\Task\Repository\TaskRepository;
use App\Infrastructure\Doctrine\Interfaces\TransactionInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TransferTaskToExecutorDoneCommand extends Command
{
    private $transaction;
    /** @var TaskRepository */
    private $taskRepository;

    public function __construct(TaskRepository $taskRepository, TransactionInterface $transaction)
    {
        parent::__construct('cli:to-executor-done');

        $this->transaction = $transaction;
        $this->taskRepository = $taskRepository;
    }

    protected function configure()
    {
        $this->addOption('taskId', 't', InputOption::VALUE_REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $taskId = $input->getOption('taskId');
        $task = $this->taskRepository->findOneById($taskId);
        $task->transferToExecutorDone();

        $this->transaction->transactional(function () use ($task) {
            $this->taskRepository->save($task);
        });

        $output->writeln("Task {$task->getId()} successfully transfered!");
    }
}

