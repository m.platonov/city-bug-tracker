<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 21.07.19
 * Time: 1:20
 */

namespace App\Application\Http\Auth;


use App\Application\Cqs\User\Command\CreateUserCommand;
use App\Application\Cqs\User\Input\CreateUserInput;
use Symfony\Component\Routing\Annotation\Route;

class SignupController
{
    /**
     * @Route("/signup", methods={"POST"})
     */
    public function create(CreateUserCommand $command, CreateUserInput $input)
    {
        return $command->execute($input);
    }
}