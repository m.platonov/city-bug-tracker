<?php


namespace App\Application\Http\User;


use App\Application\Cqs\User\Command\UpdateProfileCommand;
use App\Application\Cqs\User\Input\UpdateProfileInput;
use App\Application\Cqs\User\Output\UserOutput;
use App\Application\Cqs\User\Query\GetUsersQuery;
use App\Domain\User\Entity\User;
use Symfony\Component\Routing\Annotation\Route;

class UserController
{
    /**
     * @Route("/users/current", methods={"GET"})
     */
    public function get(User $user)
    {
        return UserOutput::from($user);
    }

    /**
     * @Route("/users", methods={"GET"})
     */
    public function getAll(GetUsersQuery $query, int $limit = 10, ?int $offset = 0)
    {
        return $query->execute($limit, $offset);
    }

    /**
     * @Route("/users/current/profile", methods={"PUT"})
     */
    public function updateProfile(UpdateProfileInput $input, UpdateProfileCommand $command)
    {
        return $command->execute($input);
    }
}
