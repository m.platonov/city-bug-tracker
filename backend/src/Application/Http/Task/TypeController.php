<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 20.07.19
 * Time: 21:39
 */

namespace App\Application\Http\Task;


use App\Application\Cqs\Task\Query\GetTypesQuery;
use Symfony\Component\Routing\Annotation\Route;

class TypeController
{
    /**
     * @Route("/types", methods={"GET"})
     */
    public function findAll(GetTypesQuery $query)
    {
        return $query->execute();
    }
//
//    /**
//     * @Route("/tasks/{status}", methods={"GET"})
//     */
//    public function getList(string $userId, GetUserQuery $query) //TODO: !
//    {
//        return $query->execute($userId);
//    }
//
//    /**
//     * @Route("/tasks/{userId}/", methods={"GET"})
//     */
//    public function getForUser(string $userId, GetUserQuery $query) //TODO: !
//    {
//        return $query->execute($userId);
//    }
}