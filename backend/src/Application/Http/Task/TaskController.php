<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 20.07.19
 * Time: 21:39
 */

namespace App\Application\Http\Task;


use App\Application\Cqs\Task\Command\CreateTaskCommand;
use App\Application\Cqs\Task\Command\FollowTaskCommand;
use App\Application\Cqs\Task\Command\RateTaskCommand;
use App\Application\Cqs\Task\Input\CreateTaskInput;
use App\Application\Cqs\Task\Input\RateTaskInput;
use App\Application\Cqs\Task\Query\GetMyTasksQuery;
use App\Application\Cqs\Task\Query\GetPublicTasksQuery;
use App\Application\Cqs\Task\Query\GetTaskByIdQuery;
use App\Application\Cqs\Task\Query\GetTasksByStatusQuery;
use Symfony\Component\Routing\Annotation\Route;

class TaskController
{
    /**
     * @Route("/tasks", methods={"POST"})
     */
    public function create(CreateTaskCommand $command, CreateTaskInput $input)
    {
        return $command->execute($input);
    }

    /**
     * @Route("/tasks/rate", methods={"POST"})
     */
    public function rate(RateTaskCommand $command, RateTaskInput $input)
    {
        return $command->execute($input);
    }

    /**
     * @Route("/tasks", methods={"GET"})
     */
    public function getListByStatus(GetTasksByStatusQuery $query, string $status, int $limit = 10, ?int $offset = 0)
    {
        return $query->execute($status, $limit, $offset);
    }

    /**
     * @Route("/task/{taskId}", methods={"GET"})
     */
    public function getTaskById(string $taskId, GetTaskByIdQuery $query)
    {
        return $query->execute($taskId);
    }

    /**
     * @Route("/tasks/public", methods={"GET"})
     */
    public function getPublicTasks(GetPublicTasksQuery $query, string $status, int $limit = 10, ?int $offset = 0)
    {
        return $query->execute($status, $limit, $offset);
    }

    /**
     * @Route("/tasks/my", methods={"GET"})
     */
    public function getMyTasks(GetMyTasksQuery $query, string $status, int $limit = 10, ?int $offset = 0)
    {
        return $query->execute($status, $limit, $offset);
    }

    /**
     * @Route("/tasks/follow/{taskId}", methods={"POST"})
     */
    public function followTaskById(string $taskId, FollowTaskCommand $command)
    {
        return $command->execute($taskId);
    }
}
