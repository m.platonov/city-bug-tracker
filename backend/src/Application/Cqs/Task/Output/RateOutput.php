<?php

namespace App\Application\Cqs\Task\Output;


use App\Domain\Task\Entity\Rating;
use App\Infrastructure\Common\Traits\Convertible;
use Ramsey\Uuid\Uuid;

class RateOutput
{
    use Convertible;

    /**
     * @var Uuid
     */
    public $id;

    /**
     * @var int
     */
    public $score;

    public $createdAt;

    public static function from(Rating $rating)
    {
        $self = new self();
        $self->id = $rating->getId();
        $self->score = $rating->getScore();
        $self->createdAt = $rating->getCreatedAt()->getTimestamp();

        return $self;
    }
}
