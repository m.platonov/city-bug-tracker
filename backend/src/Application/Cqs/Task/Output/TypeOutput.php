<?php

namespace App\Application\Cqs\Task\Output;


use App\Domain\Task\Entity\Type;
use App\Infrastructure\Common\Traits\Convertible;
use Ramsey\Uuid\Uuid;

class TypeOutput
{
    use Convertible;
    /**
     * @var Uuid
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /** @var boolean */
    public $isActual;

    public static function from(Type $type)
    {
        $self = new self();
        $self->id = $type->getId();
        $self->name = $type->getName();
        $self->isActual = $type->isActual();

        return $self;
    }
}
