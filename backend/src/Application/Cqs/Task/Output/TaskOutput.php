<?php

namespace App\Application\Cqs\Task\Output;


use App\Application\Cqs\User\Output\UserOutput;
use App\Domain\Task\Entity\Task;
use App\Domain\User\Entity\CitizenUser;
use App\Domain\User\Entity\User;
use App\Infrastructure\Common\Traits\Convertible;

class TaskOutput
{
    use Convertible;

    public $id;
    public $name;
    public $status;
    public $type;
    public $description;
    public $createdAt;
    public $creator;
    public $deadlineAt;
    public $executor;
    public $followerList;
    /** @var null|int */
    public $currentUserScore;
    public $scoreCount;
    public $scoreTotal;

    public static function from(Task $task, User $user = null)
    {
        $self = new self();
        $self->id = $task->getId();
        $self->status = $task->getStatus();
        $self->type = TypeOutput::from($task->getType());
        $self->name = $task->getName();
        $self->description = $task->getDescription();
        $self->creator = UserOutput::from($task->getCreator());
        $self->followerList = UserOutput::toArray($task->getFollowerList());

        $self->deadlineAt = $task->getDeadlineAt() === null ? null : $task->getDeadlineAt()->getTimestamp();
        $self->createdAt = $task->getCreatedAt()->getTimestamp();
        $self->executor = $task->getExecutor() === null ? null : UserOutput::from($task->getExecutor());
        if ($user){
            $self->currentUserScore = $task->getRateForUser($user) === null ? null : $task->getRateForUser($user)->getScore();
        }

        $self->scoreCount = $task->getScoreCount();
        $self->scoreTotal = $task->getScoreSum();

        return $self;
    }
}
