<?php

namespace App\Application\Cqs\Task\Input;


use Symfony\Component\Validator\Constraints as Assert;

class RateTaskInput
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    public $taskId;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\LessThanOrEqual(value="4")
     * @Assert\GreaterThanOrEqual(value="1")
     */
    public $score;
}
