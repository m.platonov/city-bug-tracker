<?php

namespace App\Application\Cqs\Task\Input;


use Symfony\Component\Validator\Constraints as Assert;

class CreateTaskInput
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Uuid()
     */
    public $typeId;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 6)
     */
    public $name;

    /**
     * @var string
     */
    public $description;
}
