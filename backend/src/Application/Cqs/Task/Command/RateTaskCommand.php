<?php

namespace App\Application\Cqs\Task\Command;


use App\Application\Cqs\Task\Input\RateTaskInput;
use App\Application\Cqs\Task\Output\TaskOutput;
use App\Domain\Task\Entity\Task;
use App\Domain\Task\Repository\RatingRepository;
use App\Domain\Task\Repository\TaskRepository;
use App\Domain\Task\Repository\TypeRepository;
use App\Domain\User\Entity\CitizenUser;
use App\Infrastructure\Doctrine\Interfaces\TransactionInterface;
use App\Infrastructure\Security\LoggedUserProvider;

class RateTaskCommand
{
    /** @var TaskRepository */
    private $taskRepository;
    /** @var LoggedUserProvider */
    private $loggedUserProvider;
    /** @var TypeRepository */
    private $typeRepository;
    /** @var RatingRepository */
    private $ratingRepository;
    private $transaction;

    public function __construct(
        TaskRepository $taskRepository,
        LoggedUserProvider $loggedUserProvider,
        TypeRepository $typeRepository,
        RatingRepository $ratingRepository,
        TransactionInterface $transaction
    )
    {
        $this->taskRepository = $taskRepository;
        $this->loggedUserProvider = $loggedUserProvider;
        $this->typeRepository = $typeRepository;
        $this->ratingRepository = $ratingRepository;
        $this->transaction = $transaction;
    }

    public function execute(RateTaskInput $input)
    {
        $user = $this->loggedUserProvider->provideEntity();
        if (!$user instanceof CitizenUser) {
            throw new \Exception('Wrong user type');
        }
        $task = $this->taskRepository->findOneById($input->taskId);
        if ($task->getStatus() !== Task::STATUS_EXECUTOR_DONE) {
            throw new \Exception('Wrong task status');
        }
        if (!is_null($task->getRateForUser($user))) {
            throw new \Exception('Already rated');
        }

        $task->addRating($user, $input->score);

        $this->transaction->transactional(function () use ($task) {
            $this->taskRepository->save($task);
        });

        return TaskOutput::from($task, $user);
    }
}
