<?php

namespace App\Application\Cqs\Task\Command;


use App\Application\Cqs\Task\Output\TaskOutput;
use App\Domain\Task\Repository\TaskRepository;
use App\Domain\Task\Service\TaskStatusTransferService;
use App\Domain\User\Entity\CitizenUser;
use App\Infrastructure\Doctrine\Interfaces\TransactionInterface;
use App\Infrastructure\Security\LoggedUserProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FollowTaskCommand
{
    /** @var TaskRepository */
    private $taskRepository;
    /** @var LoggedUserProvider */
    private $loggedUserProvider;
    private $transaction;
    /** @var TaskStatusTransferService */
    private $taskStatusTransferService;

    public function __construct(
        TaskRepository $taskRepository,
        LoggedUserProvider $loggedUserProvider,
        TaskStatusTransferService $taskStatusTransferService,
        TransactionInterface $transaction
    )
    {
        $this->taskRepository = $taskRepository;
        $this->loggedUserProvider = $loggedUserProvider;
        $this->transaction = $transaction;
        $this->taskStatusTransferService = $taskStatusTransferService;
    }

    public function execute(string $taskId)
    {
        $user = $this->loggedUserProvider->provideEntity();
        if (!$user instanceof CitizenUser) {
            throw new \Exception(); //TODO!
        }

        $task = $this->taskRepository->findOneById($taskId);

        if (is_null($task)) {
            throw new NotFoundHttpException('Task not found');
        }

        if ($task->getCreator()->getId() === $user->getId()) {
            throw new \Exception('You cannot follow your task');
        }

        $this->taskStatusTransferService->addFollowerAndTransferToExecute($task, $user);

        $this->transaction->transactional(function () use ($task) {
            $this->taskRepository->save($task);
        });

        return TaskOutput::from($task, $user);
    }
}
