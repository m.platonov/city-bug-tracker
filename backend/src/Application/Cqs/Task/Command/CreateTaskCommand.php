<?php

namespace App\Application\Cqs\Task\Command;


use App\Application\Cqs\Task\Output\TaskOutput;
use App\Application\Cqs\Task\Input\CreateTaskInput;
use App\Domain\Task\Entity\Task;
use App\Domain\Task\Exceptions\TypeNotFoundException;
use App\Domain\Task\Repository\TaskRepository;
use App\Domain\Task\Repository\TypeRepository;
use App\Domain\User\Entity\CitizenUser;
use App\Infrastructure\Doctrine\Interfaces\TransactionInterface;
use App\Infrastructure\Security\LoggedUserProvider;

class CreateTaskCommand
{
    /** @var TaskRepository */
    private $taskRepository;
    /** @var LoggedUserProvider */
    private $loggedUserProvider;
    /** @var TypeRepository */
    private $typeRepository;
    private $transaction;

    public function __construct(
        TaskRepository $taskRepository,
        LoggedUserProvider $loggedUserProvider,
        TypeRepository $typeRepository,
        TransactionInterface $transaction
    )
    {
        $this->taskRepository = $taskRepository;
        $this->loggedUserProvider = $loggedUserProvider;
        $this->typeRepository = $typeRepository;
        $this->transaction = $transaction;
    }

    public function execute(CreateTaskInput $input)
    {
        $user = $this->loggedUserProvider->provideEntity();
        if (!$user instanceof CitizenUser) {
            throw new \Exception(); //TODO!
        }
        $type = $this->typeRepository->findOneById($input->typeId);
        if (is_null($type)) {
            throw new TypeNotFoundException("Type with id {$input->typeId} is not found.");
        }

        $task = new Task($type, $input->name, $user, $input->description);
        $this->transaction->transactional(function () use ($task) {
            $this->taskRepository->add($task);
        });

        return TaskOutput::from($task, $user);
    }
}
