<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 21.07.19
 * Time: 1:51
 */

namespace App\Application\Cqs\Task\Query;


use App\Application\Cqs\Task\Output\TaskOutput;
use App\Domain\Task\Repository\TaskRepository;
use App\Domain\User\Entity\CitizenUser;
use App\Infrastructure\Security\LoggedUserProvider;

class GetPublicTasksQuery
{
    private $taskRepository;
    /** @var LoggedUserProvider */
    private $loggedUserProvider;

    public function __construct(
        TaskRepository $taskRepository,
        LoggedUserProvider $loggedUserProvider
    )
    {
        $this->taskRepository = $taskRepository;
        $this->loggedUserProvider = $loggedUserProvider;
    }

    public function execute(string $status, int $limit, int $offset)
    {
        $user = $this->loggedUserProvider->provideEntity();
        if (!$user instanceof CitizenUser) {
            throw new \Exception('Wrong user type'); //TODO!
        }

        $taskList = $this->taskRepository->findAllPublicByStatus($user, $status, $limit, $offset);
        return TaskOutput::toPaginated($taskList);
    }
}
