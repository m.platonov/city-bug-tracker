<?php

namespace App\Application\Cqs\Task\Query;


use App\Application\Cqs\Task\Output\TypeOutput;
use App\Domain\Task\Entity\Type;
use App\Domain\Task\Repository\TypeRepository;

class GetTypesQuery
{
    private $typeRepository;

    public function __construct(TypeRepository $typeRepository)
    {
        $this->typeRepository = $typeRepository;
    }

    public function execute(): array
    {
        $typeList = $this->typeRepository->findAll();
        return TypeOutput::toArray($typeList);
    }
}
