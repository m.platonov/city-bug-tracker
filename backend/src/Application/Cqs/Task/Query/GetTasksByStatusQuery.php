<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 21.07.19
 * Time: 1:51
 */

namespace App\Application\Cqs\Task\Query;


use App\Application\Cqs\Task\Output\TaskOutput;
use App\Domain\Task\Repository\TaskRepository;

class GetTasksByStatusQuery
{
    private $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function execute(string $status, int $limit, int $offset)
    {
        $taskList = $this->taskRepository->findByStatus($status, $limit, $offset);
        return TaskOutput::toPaginated($taskList);
    }
}
