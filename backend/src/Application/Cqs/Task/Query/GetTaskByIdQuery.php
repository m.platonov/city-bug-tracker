<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 21.07.19
 * Time: 1:51
 */

namespace App\Application\Cqs\Task\Query;


use App\Application\Cqs\Task\Output\TaskOutput;
use App\Domain\Task\Repository\TaskRepository;
use App\Infrastructure\Security\LoggedUserProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GetTaskByIdQuery
{
    private $taskRepository;
    /** @var LoggedUserProvider */
    private $loggedUserProvider;

    public function __construct(
        TaskRepository $taskRepository,
        LoggedUserProvider $loggedUserProvider
    )
    {
        $this->taskRepository = $taskRepository;
        $this->loggedUserProvider = $loggedUserProvider;
    }

    public function execute(string $taskId): TaskOutput
    {
        $user = $this->loggedUserProvider->provideEntity();
        $task = $this->taskRepository->findOneById($taskId);

        if (is_null($task)) {
            throw new NotFoundHttpException("Task not found");
        }

        return TaskOutput::from($task, $user);
    }
}