<?php

namespace App\Application\Cqs\User\Output;


use App\Domain\User\Entity\User;
use App\Infrastructure\Common\Traits\Convertible;
use Ramsey\Uuid\Uuid;

class UserOutput
{
    use Convertible;
    /**
     *
     * @var Uuid
     */
    public $id;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $login;

    /**
     * @var string
     */
    public $email;

    /**
     * @var \DateTimeImmutable
     */
    public $createdAt;

    /**
     * @var \DateTimeImmutable|null
     */
    public $updatedAt;

    public static function from(User $user)
    {
        $self = new self();
        $self->id = $user->getId();
        $self->username = $user->getUsername();
        $self->login = $user->getLogin();
        $self->email = $user->getEmail();
        $self->createdAt = $user->getCreatedAt()->getTimestamp();
        $self->updatedAt = $user->getUpdatedAt() === null ? null : $user->getUpdatedAt()->getTimestamp();

        return $self;
    }
}
