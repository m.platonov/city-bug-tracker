<?php
/**
 * Created by PhpStorm.
 * Date: 21.07.19
 * Time: 8:21
 */

namespace App\Application\Cqs\User\Input;

use Symfony\Component\Validator\Constraints as Assert;

class UpdateProfileInput
{
    /**
     * @var string
     *
     * @Assert\Type(type="string")
     */
    public $username;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     */
    public $email;
}
