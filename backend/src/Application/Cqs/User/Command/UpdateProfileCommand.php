<?php
/**
 * Created by PhpStorm.
 * Date: 21.07.19
 * Time: 8:22
 */

namespace App\Application\Cqs\User\Command;

use App\Application\Cqs\User\Input\UpdateProfileInput;
use App\Application\Cqs\User\Output\UserOutput;
use App\Domain\User\Exceptions\UserNotFoundException;
use App\Infrastructure\Doctrine\Interfaces\TransactionInterface;
use App\Infrastructure\Security\LoggedUserProvider;

class UpdateProfileCommand
{
    /**
     * @var LoggedUserProvider
     */
    private $loggedUserProvider;
    /**
     * @var TransactionInterface
     */
    private $transaction;

    public function __construct(
        LoggedUserProvider $loggedUserProvider,
        TransactionInterface $transaction
    ) {
        $this->loggedUserProvider = $loggedUserProvider;
        $this->transaction = $transaction;
    }

    public function execute(UpdateProfileInput $input)
    {
        $user = $this->loggedUserProvider->provideEntity();
        if (!$user) {
            throw new UserNotFoundException('Пользователь не найден');
        }
        $this->transaction->transactional(static function () use ($user, $input) {
            $user->changeUsername($input->username);
            $user->changeEmail($input->email);
        });
        return UserOutput::from($user);
    }
}
