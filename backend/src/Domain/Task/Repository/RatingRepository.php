<?php

namespace App\Domain\Task\Repository;


use App\Domain\Task\Entity\Rating;
use App\Domain\Task\Exceptions\RatingAlreadyAddedException;
use App\Infrastructure\Doctrine\Repository\BaseDoctrineRepository;

class RatingRepository extends BaseDoctrineRepository
{
    public function add(Rating $rating)
    {
        if ($this->entityManager->contains($rating)) {
            throw new RatingAlreadyAddedException("Rating with id {$rating->getId()} is already added.");
        }

        $this->entityManager->persist($rating);
    }
}
