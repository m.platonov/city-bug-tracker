<?php

namespace App\Domain\Task\Repository;


use App\Domain\Task\Entity\Type;
use App\Infrastructure\Doctrine\Repository\BaseDoctrineRepository;

class TypeRepository extends BaseDoctrineRepository
{
    public function findAll()
    {
        return $this->entityManager->createQueryBuilder()
            ->from(Type::class, 'type')
            ->select('type')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $id
     * @return Type|null
     */
    public function findOneById(string $id)
    {
        /** @var Type|null $type */
        $type = $this->entityManager->getRepository(Type::class)->findOneBy(['id' => $id]);

        return $type;
    }
}
