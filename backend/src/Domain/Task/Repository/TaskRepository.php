<?php

namespace App\Domain\Task\Repository;


use App\Domain\Task\Entity\Task;
use App\Domain\Task\Exceptions\TaskAlreadyAddedException;
use App\Domain\User\Entity\CitizenUser;
use App\Infrastructure\Doctrine\Repository\BaseDoctrineRepository;

class TaskRepository extends BaseDoctrineRepository
{
    public function add(Task $task)
    {
        if ($this->entityManager->contains($task)) {
            throw new TaskAlreadyAddedException("Task with id {$task->getId()} is already added.");
        }

        $this->entityManager->persist($task);
    }

    public function save(Task $task)
    {
        $this->entityManager->flush($task);
    }

    /**
     * @param string $status
     * @param $limit
     * @param $offset
     * @return Task[]
     */
    public function findByStatus(string $status, $limit, $offset): array
    {
        return $this->entityManager->createQueryBuilder()
            ->from(Task::class, 'task')
            ->select('task')
            ->where('task.status = :status')
            ->setParameter('status', $status)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findOneById(string $taskId): ?Task
    {
        /** @var Task|null $task */
        $task = $this->entityManager->getRepository(Task::class)->find($taskId);

        return $task;
    }

    /**
     * @param CitizenUser $currentUser
     * @param string $status
     * @param int $limit
     * @param int $offset
     * @return Task[]
     */
    public function findAllPublicByStatus(CitizenUser $currentUser, string $status, int $limit, int $offset): array
    {
        $qb = $this->entityManager->createQueryBuilder();

        return $qb
            ->from(Task::class, 'task')
            ->select('task')
            ->where('task.status = :status')
            ->andWhere($qb->expr()->neq('task.creator', ':currentUserId'))
            ->setParameter('status', $status)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->setParameter('currentUserId', $currentUser->getId())
            ->getQuery()
            ->getResult();
    }

    public function findAllMyByStatus(CitizenUser $currentUser, string $status, int $limit, int $offset)
    {
        $qb = $this->entityManager->createQueryBuilder();

        return $qb
            ->from(Task::class, 'task')
            ->select('task')
            ->where('task.status = :status')
            ->andWhere($qb->expr()->eq('task.creator', ':currentUserId'))
            ->setParameter('status', $status)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->setParameter('currentUserId', $currentUser->getId())
            ->getQuery()
            ->getResult();
    }
}
