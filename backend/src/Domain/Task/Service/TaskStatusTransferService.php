<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 21.07.19
 * Time: 3:56
 */

namespace App\Domain\Task\Service;


use App\Domain\Task\Entity\Task;
use App\Domain\Task\Repository\TypeRepository;
use App\Domain\User\Entity\CitizenUser;
use App\Domain\User\Repository\UserRepository;

class TaskStatusTransferService
{
    /** @var TypeRepository */
    private $typeRepository;
    /** @var UserRepository */
    private $userRepository;

    public function __construct(TypeRepository $typeRepository, UserRepository $userRepository)
    {
        $this->typeRepository = $typeRepository;
        $this->userRepository = $userRepository;
    }

    public function addFollowerAndTransferToExecute(Task $task, CitizenUser $user)
    {
        $task->addFollower($user);

        if (count($task->getFollowerList()) > 0) {
            $type = $task->getType();
            $executorList = $this->userRepository->findAllExecutorsWithType($type);

            $task->transferToAssigned($executorList[0]);
        }
    }
}