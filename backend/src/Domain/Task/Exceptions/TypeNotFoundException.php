<?php

namespace App\Domain\Task\Exceptions;


use App\Infrastructure\Doctrine\Exceptions\RepositoryException;

class TypeNotFoundException extends RepositoryException
{

}
