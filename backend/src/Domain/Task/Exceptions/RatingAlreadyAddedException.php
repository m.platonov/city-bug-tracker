<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 21.07.19
 * Time: 23:26
 */

namespace App\Domain\Task\Exceptions;


use App\Infrastructure\Doctrine\Exceptions\RepositoryException;

class RatingAlreadyAddedException extends RepositoryException
{

}