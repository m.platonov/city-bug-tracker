<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 20.07.19
 * Time: 22:03
 */

namespace App\Domain\Task\Entity;


use App\Domain\Common\Traits\CreatedAt;
use App\Domain\Common\Traits\Entity;
use App\Domain\User\Entity\CitizenUser;
use App\Domain\User\Entity\ExecutorUser;
use App\Domain\User\Entity\User;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;

class Task
{
    use Entity, CreatedAt;

    const STATUS_DRAFT = 'draft';
    const STATUS_VOTING = 'voting';
    const STATUS_ASSIGNED = 'assigned';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_EXECUTOR_DONE = 'executor_done';
    const STATUS_CONTROL = 'control';
    const STATUS_ARCHIVE = 'archive';

    /** @var string */
    protected $status;
    /** @var Type */
    protected $type;
    /** @var string */
    protected $name;
    /** @var string|null */
    protected $description;
    /** @var CitizenUser */
    protected $creator;
    /** @var CitizenUser[]|ArrayCollection */
    protected $followerList;
    /** @var DateTimeImmutable|null */
    protected $deadlineAt;
    /** @var Rating[] */
    protected $rateList;
    /** @var ExecutorUser|null */
    protected $executor;

    public function __construct(Type $type, string $name, CitizenUser $creator, string $description = null)
    {
        $this->identify();
        $this->onCreated();

        $this->status = self::STATUS_DRAFT;

        $this->type = $type;
        $this->name = $name;
        $this->description = $description;
        $this->creator = $creator;
        $this->followerList = new ArrayCollection();
        $this->rateList = new ArrayCollection();
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getCreator(): CitizenUser
    {
        return $this->creator;
    }

    public function getFollowerList(): array
    {
        return $this->followerList->toArray();
    }

    public function addFollower(CitizenUser $user)
    {
        $this->followerList->add($user);
    }

    public function getDeadlineAt(): ?DateTimeImmutable
    {
        return $this->deadlineAt;
    }

    public function getExecutor(): ?ExecutorUser
    {
        return $this->executor;
    }

    public function transferToAssigned(ExecutorUser $executor)
    {
        $this->executor = $executor;
        $this->status = self::STATUS_ASSIGNED;
    }

    public function transferToInProgress()
    {
        $this->status = self::STATUS_IN_PROGRESS;
    }

    public function transferToExecutorDone()
    {
        $this->status = self::STATUS_EXECUTOR_DONE;
    }

    public function addRating(CitizenUser $user, int $score)
    {
        $rating = new Rating($score, $user, $this);

        $this->rateList->add($rating);
    }

    public function getRateForUser(User $user): ?Rating
    {
        $criteria = new Criteria();
        $userExpr = new Comparison('user', Comparison::EQ, $user);
        $criteria->andWhere($userExpr);

        if ($this->rateList->matching($criteria)->count() > 0) {
            return $this->rateList->matching($criteria)->first();
        }

        return null;
    }

    public function getScoreCount(): int
    {
        return $this->rateList->count();
    }

    public function getScoreSum(): int
    {
        $result = 0;
        foreach ($this->rateList as $rate) {
            $result += $rate->getScore();
        }

        return $result;
    }
}
