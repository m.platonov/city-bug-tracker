<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 20.07.19
 * Time: 22:00
 */

namespace App\Domain\Task\Entity;


use App\Domain\Common\Traits\Entity;

class Type
{
    use Entity;

    protected $name;
    protected $isActual;
    //TODO: продолжительность

    public function __construct(string $name, bool $isActual = true)
    {
        $this->identify();
        $this->name = $name;
        $this->isActual = $isActual;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isActual(): bool
    {
        return $this->isActual;
    }
}