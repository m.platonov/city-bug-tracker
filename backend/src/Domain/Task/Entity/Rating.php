<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 21.07.19
 * Time: 12:28
 */

namespace App\Domain\Task\Entity;


use App\Domain\Common\Traits\CreatedAt;
use App\Domain\Common\Traits\Entity;
use App\Domain\User\Entity\CitizenUser;

class Rating
{
    use Entity, CreatedAt;

    /** @var int */
    protected $score;
    /** @var CitizenUser */
    protected $user;
    /** @var Task */
    protected $task;

    /**
     * Rating constructor.
     * @param int $score
     * @param CitizenUser $user
     * @param Task $task
     */
    public function __construct(int $score, CitizenUser $user, Task $task)
    {
        $this->identify();
        $this->onCreated();

        $this->score = $score;
        $this->user = $user;
        $this->task = $task;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }
}