<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 20.07.19
 * Time: 22:02
 */

namespace App\Domain\User\Entity;


use App\Domain\Task\Entity\Type;
use Doctrine\Common\Collections\ArrayCollection;

class ExecutorUser extends User
{
    //TODO: geo

    /** @var Type[]|ArrayCollection */
    protected $typeList;

    public function __construct(string $username, string $login, string $password)
    {
        parent::__construct($username, $login, $password);

        $this->typeList = new ArrayCollection();
    }

    /**
     * @return Type[]
     */
    public function getTypeList(): array
    {
        return $this->typeList->toArray();
    }
}