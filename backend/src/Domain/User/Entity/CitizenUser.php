<?php
/**
 * Created by
 * User: Platonov Mikhail(m.platonov@s-cabinet.ru)
 * Date: 20.07.19
 * Time: 21:52
 */

namespace App\Domain\User\Entity;


class CitizenUser extends User
{
    /** @var float */
    protected $rating;

    public function __construct(string $username, string $login, string $password)
    {
        $this->rating = 0;

        parent::__construct($username, $login, $password);
    }

    public function getRating(): float
    {
        return $this->rating;
    }

    public function increaseRating()
    {
        $this->rating += 0.1;
    }
}