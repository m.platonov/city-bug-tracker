<?php

namespace App\Domain\User\Entity;


use App\Domain\Common\Traits\CreatedAt;
use App\Domain\Common\Traits\Entity;
use App\Domain\Common\Traits\UpdatedAt;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class User implements UserInterface
{
    use Entity, CreatedAt, UpdatedAt;

    private $username;
    private $login;
    private $email;
    private $password;
    private $isActivated = true;

    public function __construct(
        string $username,
        string $login,
        string $password
    ) {
        $this->identify();
        $this->onCreated();

        $this->username = $username;
        $this->login = $login;
        $this->password = $password;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function changeUsername(string $username)
    {
        $this->username = $username;
        $this->onUpdated();
    }

    public function changeEmail(string $email)
    {
        $this->email = $email;
        $this->onUpdated();
    }

    public function changePassword(string $password)
    {
        $this->password = $password;
        $this->onUpdated();
    }

    public function isActivated(): bool
    {
        return $this->isActivated;
    }

    public function deactivate()
    {
        $this->isActivated = false;
        $this->onUpdated();
    }

    public function activate()
    {
        $this->isActivated = true;
        $this->onUpdated();
    }

    public function getRoles()
    {
        return [];
    }

    public function getSalt()
    {
        return;
    }

    public function eraseCredentials()
    {
        return;
    }
}
