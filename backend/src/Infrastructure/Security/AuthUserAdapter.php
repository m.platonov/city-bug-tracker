<?php

namespace App\Infrastructure\Security;


use App\Domain\User\Entity\User;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;


class AuthUserAdapter implements UserInterface
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getId(): Uuid
    {
        return $this->user->getId();
    }

    public function getRoles()
    {
        return [];
    }

    public function getPassword()
    {
        return $this->user->getPassword();
    }

    public function getUsername()
    {
        return $this->user->getLogin();
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    public function unwrap(): User
    {
        return $this->user;
    }
}
