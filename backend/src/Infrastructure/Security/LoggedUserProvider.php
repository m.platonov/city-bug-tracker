<?php

namespace App\Infrastructure\Security;


use App\Domain\User\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LoggedUserProvider
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * ДОЛЖЕН вернуть текущего пользователя завернутого в security адаптер.
     *
     * @return AuthUserAdapter
     *     Адаптер авторизованного пользователя.
     *
     * @throws LogicException
     *     Если метод будет вызван во время анонимной сессии, cli скрипта или в прочем контексте где недоступен пользователь.
     */
    public function requireAdapter(): AuthUserAdapter
    {
        $adapter = $this->provideAdapter();

        if (null !== $adapter) {
            return $adapter;
        }

        throw new LogicException('You attempt to require security adapter during anonymous session.');
    }

    /**
     * Пытается вернуть текущего пользователя завернутого в security адаптер.
     *
     * @return AuthUserAdapter|null
     *     Адаптер текущего пользователя или null (при анонимной сессии, при вызове из cli и т.д.).
     */
    public function provideAdapter(): ?AuthUserAdapter
    {
        $token = $this->tokenStorage->getToken();

        if ($token instanceof JWTUserToken) {
            /** @var AuthUserAdapter $adapter */
            $adapter = $token->getUser();
            return $adapter;
        }

        return null;
    }

    /**
     * Пытается вернуть сущность текущего авторизованного пользователя.
     *
     * @return User|null
     *     Сущность пользователя или null (при анонимной сессии, при вызове из cli и т.д.).
     */
    public function provideEntity(): ?User
    {
        $adapter = $this->provideAdapter();

        if (null !== $adapter) {
            return $adapter->unwrap();
        }

        return null;
    }

    /**
     * ДОЛЖЕН вернуть сущность текущего авторизованного пользователя.
     *
     * @param string|null $entityClass
     *     Имя класса для указания ожидания конкретной сущности.
     *     Не указывается если ожидается любая сущность пользователя.
     *
     * @return User
     *     Сущность авторизованного пользователя.
     *
     * @throws LogicException
     *     Если метод будет вызван во время анонимной сессии, cli скрипта или в прочем контексте где недоступен пользователь.
     *     Если актуальный класс сущности не совпадает с ожидаемым.
     */
    public function requireEntity(string $entityClass = null): User
    {
        $adapter = $this->provideAdapter();

        if (null === $adapter) {
            throw new LogicException('You attempt to require logged user during anonymous session.');
        }

        $entity = $adapter->unwrap();

        if (null === $entityClass) {
            return $entity;
        }

        if (get_class($entity) !== $entityClass && !is_subclass_of($entity, $entityClass)) {
            throw new LogicException("You attempt to require '{$entityClass}' outside of it context.");
        }

        return $entity;
    }
}
