<?php
/**
 * Created by PhpStorm.
 * Date: 21.07.19
 * Time: 6:00
 */

namespace App\Infrastructure\Common\Traits;


use App\Infrastructure\Common\PaginatedData;

trait Convertible
{
    abstract public static function from($item): self;

    /**
     * Перевод коллекцию объектов из репозитория в output коллекцию.
     * Данная коллекция имеет данные `page` и `total`, что позволит организовать постраничный вывод.
     *
     * @param $collection
     *     Массив объектов из репозитория.
     *
     * @param array $params
     * @return PaginatedData|static[]
     *     Output коллекция с данными о пагинации.
     */
    final public static function toPaginated($collection, $params = []): PaginatedData
    {
        return new PaginatedData($outputs = self::toArray($collection, ...$params), count($outputs),
            count($collection));
    }

    /**
     * Перводи каждый переданный элемент с помощью {@see Convertible::from()}
     * в результирующий массив output объектов.
     *
     * @param object[] $items
     *     Коллекция элементов которую нужно трансформировать в output коллекцию.
     *
     * @param mixed[] $params
     *     Опциональные доп. параметры которые могут быть переданы в {@see Convertible::from()}.
     *
     * @return self[]
     *     Коллекция output объектов созданая на базе переданных объектов.
     */
    final public static function toArray(iterable $items, ...$params): array
    {
        $aggregation = [];

        foreach ($items as $entity) {
            $aggregation[] = self::{'from'}($entity, ...$params);
        }

        return $aggregation;
    }
}
