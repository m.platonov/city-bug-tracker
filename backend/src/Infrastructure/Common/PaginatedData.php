<?php

namespace App\Infrastructure\Common;

class PaginatedData
{
    public $page;

    public $total;

    public $data;

    public function __construct(array $data, int $page, int $total)
    {
        $this->data = $data;
        $this->page = $page;
        $this->total = $total;
    }
}
