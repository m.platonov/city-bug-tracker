<?php

namespace App\Infrastructure\Common;


class RequestValueCaster
{
    public static function cast($value, string $type)
    {
        if (null !== $value) {
            if ('int' === $type) {
                return (int)$value;
            }

            if ('float' === $type) {
                return (float)$value;
            }

            if ('bool' === $type) {
                $value = json_decode($value);
                return (bool)$value;
            }
        }

        return $value;
    }
}
