<?php

namespace App\Infrastructure\Http\Resolver;


use App\Infrastructure\Common\RequestValueCaster;
use App\Infrastructure\Exception\ArgumentResolvingException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Throwable;

class QueryResolver implements ArgumentValueResolverInterface
{
    private $denormalizer;

    public function __construct(DenormalizerInterface $denormalizer)
    {
        $this->denormalizer = $denormalizer;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return $request->query->has($argument->getName());
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $name = $argument->getName();
        $type = $argument->getType() ?: ''; // TODO сделать что то с этой заплаткой
        try {
            yield RequestValueCaster::cast($request->query->get($name), $type);
        } catch (Throwable $e) {
            throw ArgumentResolvingException::cannotResolveAttribute($name, $type, $e);
        }
    }
}
